import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'ListView with Search'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List _languages = ["Urdu", "Deutsch", "Original Ton", "Arabisch", "Bangalisch","Englisch","Franzosisch"];
    List<String> urdu = ["029112270100","029112270101","029112270102","029112270103"];
      List<String> deutsch = ["029112270180","029112270181"];
                 List<String> original_Ton = ["029112270190"];
                 List<String> arabisch = ["029112270191"];
                 List<String> bengalisch = ["029112270192"];
                 List<String> englisch = ["029112270193"];
                 List<String> fransozisch = ["029112270194"];

                   List<DropdownMenuItem<String>> _dropDownMenuItems;
                   String _selectedLanguage;

     List<DropdownMenuItem<String>> buildAndGetDropDownMenuItems(List languages)
     {
       List<DropdownMenuItem<String>> items = List();
        for (String language in languages) {
            items.add(DropdownMenuItem(value: language, child: Text(language)));
         }
         return items;
     }

     void changedDropDownItem(String selectedLanguage)
     {                 
       setState(() {
         _selectedLanguage = selectedLanguage;
         items.clear();
         if(_selectedLanguage == "Urdu")
           {
              items.addAll(urdu);
           }
         else if(_selectedLanguage == "Deutsch")
           {
             items.addAll(deutsch);
           }
           else if(_selectedLanguage == "Arabisch") {items.addAll(arabisch);}
           else if(_selectedLanguage == "Original Ton"){items.addAll(original_Ton);}
           else if(_selectedLanguage == "Bangalisch")  {items.addAll(bengalisch);}
           else if(_selectedLanguage == "Englisch") {items.addAll(englisch);}
           else if(_selectedLanguage == "Franzosisch") {items.addAll(fransozisch);}
       });
     }

  TextEditingController editingController = TextEditingController();

  final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items = List<String>();

  @override
  void initState() {
   _dropDownMenuItems = buildAndGetDropDownMenuItems(_languages);
   _selectedLanguage = _dropDownMenuItems[0].value;
    items.addAll(urdu);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("MTA Phonecast"),
        centerTitle: true,
                        backgroundColor: Color.fromARGB(255, 4, 3, 32),
      ),



      body: Container(

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,



          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),

              child: Text("Wähle Tonspur", style: TextStyle(color: Colors.white,),),

            ),

            Padding(

              padding: const EdgeInsets.all(0.0),

              /*child: TextField(
                onChanged: (value) {

                },
                controller: editingController,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25.0)))),
              ),*/



              child: DropdownButton (

                style: TextStyle(color: Colors.black, decorationColor: Colors.white, fontSize: 16, ),
              isExpanded: true,
             iconEnabledColor: Colors.white,
             value: _selectedLanguage,
              items: _dropDownMenuItems,


             /*   items: <String>["Urdu", "Deutsch", "Original Ton", "Arabisch", "Bangalisch","Englisch","Franzosisch"]
                    .map<DropdownMenuItem<String>>((String selectedLanguage) {
                  return DropdownMenuItem<String>(
                      //child: Text(value),
                      child: Text(
                      selectedLanguage,
                      style: TextStyle(
                      color: Colors.black,

                  ),
                      ),

                  );
                })
                    .toList(),*/
                onChanged: changedDropDownItem,

                hint: Text(
                'asdasd',
                  style: TextStyle(color: Colors.white),
                ),





              ),

            ),
            Expanded(


              child: ListView.builder(

                shrinkWrap: true,
                itemCount: items.length,


                itemBuilder: (context, index) {


                    return Card (
                     color: Colors.white,


                      child: ListTile(
                    title: Text('${items[index]}', ),
                    trailing: Icon(Icons.phone),


                    onTap : ()
                      {
                        launch("tel:${items[index]}");
                      }



                  ),

                  );

                },
              ),
            ),
          ],

        ),
      ),
      backgroundColor: Color.fromARGB(255,20,60,181),

      //backgroundColor: Colors.blue.shade900,

    );
  }
}